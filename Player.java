package xo;

public class Player {
	private char name;
	private int Win;
	private int Lose;
	private int Draw;
	
	public Player(char name) {
		this.name = name;
		
	}
	public char getName() {
		return name;
	}
	public void setName(char name) {
		this.name = name;
	}
	public int getWin() {
		return Win;
	}
	
	public int getLose() {
		return Lose;
	}
	
	public int getDraw() {
		return Draw;
	}
	
	public void addWin() {
		Win++;
	}
	
	public void addLose() {
		Lose++;
	}
	
	public void addDraw() {
		Draw++;
	}
	

	
}
