package xo;

import java.util.Scanner;
import java.util.InputMismatchException;
public class Game {//รับและแสดงผลหน้าจอ
	private Table table;	
	private int row;
	private int col;
	private Player o;
	private Player x;
	Scanner kb = new Scanner(System.in);
	
	public Game() {
		this.o = new Player('o');
		this.x = new Player('x');
	}
		
	public  void  startGame() { //สร้าง ตาราง
		if(getRandomNumber(1,100)%2==0) {
			this.table = new Table(o,x);
		}else {
			this.table = new Table(x,o);
		}
		}
	
	public   void  showWelcome() { 
		System.out.println("|--->>WELCOME TO OX GAME<<---|");
	}
	
	public  void  showTable() {
		char[][] data = this.table.getData();
		 for(int row=0;row<data.length;row++){			
            for(int col=0;col<data.length;col++){
                System.out.print(data[row][col]);
            }
            	System.out.println();
            }
	}
	
	public  void  showTurn() { 
		System.out.println(table.getCurrentPlayer().getName()+" Turn");
	}
	
	public void input() {
		while(true) {
			try {
				System.out.print("Please input Row|Column: ");
				row = kb.nextInt();
				col = kb.nextInt();
				return;
			}catch(InputMismatchException iE) {
				kb.next();
				System.out.println("Please input number 1-3!!!");
			}
		}		
	}
	
	public  void  inputRowCol() { 
           while(true) {
				this.input();
				try {
        	   if(table.setRowCol(row, col)==true) { 		 
        		  return ;
        	   }
        	   System.out.println("NOT NULL!! PLEASE INPUT AGAIN");
				}catch(ArrayIndexOutOfBoundsException e){
				  System.out.println("Please input number 1-3!!!");
				}      	   
           }          
	}
	
	public  void  showWin() { 
		this.showTable();
		 if(table.getWinner() != null) {
			   System.out.println("<----- "+table.getWinner().getName()+" THE WINNER!!----->");	
			   this.showStack();
		   }else {
			   System.out.println("<--------- DRAW!! -------->");
			   this.showStack();
		   }		
	}
		
	public  void  showBye() { 
		System.out.println("|------------------------|");
    	System.out.println("<--------GOOD BYE-------->");
	}
	
	public void run() {
		while(true) {
			this.runOnce();
			if(! inputPlayAgain()) {
				showBye();
				return ;
			}			
		}
	}
	
	public  void  runOnce() {
		this.showWelcome();
		this.startGame();
		while(true) {
			this.showTable();
			this.showTurn();
			this.inputRowCol();
			if(table.checkWin()) {			 
			   showWin();
			   return;
			}		
			table.switchTurn();
		}
	}
	    
	private void showStack() {
		System.out.println(o.getName()+" WIN : "+o.getWin()+" LOSE : "+o.getLose()+" DRAW : "+o.getDraw());
		System.out.println(x.getName()+" WIN : "+x.getWin()+" LOSE : "+x.getLose()+" DRAW : "+x.getDraw());
	}

	public boolean  inputPlayAgain() {
		 while(true) {
	        System.out.print("Continue? (yes/no): ");
	        String ans = kb.next();
	        if (ans.equals("yes")) {
	            return true;
	        }else if(ans.equals("no")) {
	            return false;
	        }	       
		 }
	    }
		
	private int getRandomNumber(int min,int max) {
		return (int)((Math.random() * (max - min)) + min);
	}		
		
}

