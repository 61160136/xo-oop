package xo;

public class Table {//จัดการการเล่นเกมและจัดการตาราง
	char[][] data = {
			{'-', '-', '-'},
			{'-', '-', '-'},
			{'-', '-', '-'}};

	private Player currentPlayer;
	private Player o;
	private Player x;
	private Player win ;
	int row;
	int col;
	int numTurn;
		
	Table(Player o,Player x){
		this.o = o;
		this.x = x;
		this.currentPlayer = this.x;
	}
	
	public char[][] getData() {
		return data;
	}

	public   boolean setRowCol(int row, int col) { 
		if(this.data[row-1][col-1] != '-') {			
			return false;
		}else {
			data[row-1][col-1] = currentPlayer.getName();	
			numTurn++;
			return true;
		}
	}
	
	public  Player getCurrentPlayer() { 
		return currentPlayer;
	}
	
	public   boolean  checkWin() {
		if(checkRow(row) || checkCol(col) || checkDiag1() || checkDiag2()) {
		  this.win = currentPlayer;
		  updateWin();
		  return true;
		}
		if(numTurn==9) {
			updateWin();
		  return true;
		}
		return false;
	}
	
	public void updateWin() {
		if(this.win == this.x) {
			this.x.addWin();
			this.o.addLose();
		}else if(this.win == this.o) {
			this.o.addWin();
			this.x.addLose();
		}else {
			this.o.addDraw();
			this.x.addDraw();
		}

	}
	
	private boolean checkRow(int row) {
		for(int col=0;col<data.length;col++) {
			if(data[row][col] != currentPlayer.getName()) {
				return false;
			}
		}
		return true;
	}
	
	private boolean checkCol(int col) {
		for(int row=0;row<data.length;row++) {
			if(data[row][col] != currentPlayer.getName()) {
				return false;
			}
		}
		return true;
	}
	
	private boolean checkDiag1() {
		for(int i=0;i<data.length;i++) {
			if(data[i][i] != currentPlayer.getName()) {
				return false;
			}
		}
		return true;
	}
	
	private boolean checkDiag2() {
		for(int i=0;i<data.length;i++) {
			if(data[i][data.length-i-1] != currentPlayer.getName()) {
				return false;
			}
		}
		return true;
	}
	
	public   Player  getWinner() {
		return win;		
	}
	
	public void  switchTurn() { 
		if(this.currentPlayer == x) {
			this.currentPlayer = o;
		}else {
			this.currentPlayer = x;
		}
	}
	
	
	
	
	
}